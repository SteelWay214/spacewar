﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void quitGame()
    {
        SceneManager.LoadScene("mainMenu");
    }

    public void LoadLevelOne()
    {
        SceneManager.LoadScene("levelOne");
    }
     
    public void LoadLevelTwo()
    {
        SceneManager.LoadScene("levelTwo");
    }
    
    public void LoadLevelThree()
    {
        SceneManager.LoadScene("levelThree");
    }
 
    public void LoadLevelFour()
    {
        SceneManager.LoadScene("levelFour");
    }
    
    public void LoadLevelFive()
    {
        SceneManager.LoadScene("levelFive");
    }
 
    public void exitGame() 
    {
        Application.Quit();
    }
}
