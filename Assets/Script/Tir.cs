﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tir : MonoBehaviour
{
    public GameObject Projectil;
    public int force;
    public AudioClip SoundTir;

    private AudioSource _audioSource;
    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frameS
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //print("tir");
            _audioSource.PlayOneShot(SoundTir);
            GameObject Bullet = Instantiate(Projectil, transform.position, Quaternion.identity);
            Bullet.GetComponent<Rigidbody>().velocity = Vector2.up * force * Time.deltaTime;       
            Destroy(Bullet, 1f);
        }    
    }
}
