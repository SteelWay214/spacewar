﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
private Vector2 Velocity;
public float Distance = 5;
public float Speed = 2f;
private Vector2 PointDeDepart;
private float DistanceParcourue;
private bool IsGoingRight = true;

    // Start is called before the first frame update
    void Start()
    {
        Velocity = new Vector2(Speed, 0);
        PointDeDepart = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        DistanceParcourue = transform.position.x - PointDeDepart.x;
        if (IsGoingRight)
        {
            transform.Translate(Velocity.x * Time.deltaTime, 0, 0);
            if (DistanceParcourue>Distance)
            {
                transform.eulerAngles = new Vector2(0, 180);
                IsGoingRight = false;
            }
        }
        else
        {
            transform.Translate(Velocity.x * Time.deltaTime, 0 ,0);
            if (DistanceParcourue<0)
            {
                transform.eulerAngles=new Vector2(0, 360);
                IsGoingRight = true;
            }
        }
    }
}
