﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITir : MonoBehaviour
{
    public GameObject Projectil;
    public int force;
    public float FireRate = 1f;
    public float nextFire = 1f;
    public float tirDistance = 25f;
    public AudioClip SoundTir;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time>nextFire)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, Vector2.down, out hit))
            {
                if(hit.collider.gameObject.tag=="Joueur" && hit.distance<tirDistance)
                {
                    GetComponent<AudioSource>().PlayOneShot(SoundTir);
                    GameObject Bullet = Instantiate(Projectil, transform.position, Quaternion.identity) as GameObject;
                    Bullet.GetComponent<Rigidbody>().velocity = Vector2.down * force * Time.deltaTime;
                    Destroy(Bullet, 2f);
                    nextFire = Time.time + FireRate;
                }
            }       
        } 
    }
}
