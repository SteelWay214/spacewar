﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInput2 : MonoBehaviour
{
    private Button saveBtn;
    private InputField inputField;

    private void Awake()
    {
        saveBtn = transform.Find("Save2").GetComponent<Button>();
        inputField = transform.Find("PlayerField2").GetComponent<InputField>();

        inputField.characterLimit = 8;

        saveBtn.onClick.AddListener(delegate { Save(inputField.text); });
    }

    private void Save(string pseudo)
    {
        FindObjectOfType<PseudoManager>().SetPlayer2Pseudo(pseudo);
        Debug.Log(pseudo);
    }
}
