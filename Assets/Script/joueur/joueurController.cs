﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Enums;

public class joueurController : MonoBehaviour
{
    public string playerName;

    public float MaxLife = 20;
    private float Life;
    public Scrollbar LifeBar;
    private bool IsDead = false;
    
    [SerializeField]
    int PlayerID;

    [SerializeField]
    float Speed;

    ControlsManager controlsManager;

    void Start()
    {
        Life = MaxLife;
        LifeBar.size = 1;
        controlsManager = FindObjectOfType<ControlsManager>();
    }
    void Update()
    {
        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.LeftKey)))
        {
            transform.Translate(Vector2.left * Speed * Time.deltaTime);
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.RightKey)))
        {
            transform.Translate(Vector2.right * Speed * Time.deltaTime);
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.UpKey)))
        {
            transform.Translate(Vector2.up * Speed * Time.deltaTime);
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.DownKey)))
        {
            transform.Translate(Vector2.down * Speed * Time.deltaTime);
        }

        if (Input.GetKey(controlsManager.GetKey(PlayerID, ControlKeys.Action)))
        {
            Debug.Log( "" + PlayerID + " Action Fired");
        }
    }
    void OnCollisionEnter(Collision Col)
    {
        if(Col.gameObject.tag == "bullet" && !IsDead)
        {
            TakeDamages(5);
        }
    }

    private void TakeDamages(float value)
    {
        
        Life -= value;
        LifeBar.size = Life / MaxLife;
        if (Life <= 0)
        {
            Debug.Log(Life);
            Dead();
        }
    }

    private void Dead()
    {
        SceneManager.LoadScene(playerName);
        Destroy(this);
    }
}
